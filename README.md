#### 简介
swoole是PHP语言的高性能网络通信框架，提供了PHP语言的异步多线程服务器，异步TCP/UDP网络客户端，异步MySQL，数据库连接池，AsyncTask，消息队列，毫秒定时器，异步文件读写，异步DNS查询。
laravel是php的快速开发mvc全栈框架，开发速度一流，性能一般。
这个项目是把两则结合的一个研究项目，争取开发速度和性能都能完美吧。

#### 安装
1. debian8 下
```
  sudo apt-get install php5-dev php-pear
```
2. 安装swoole
```
  pecl install swoole
```
3. 编辑php.ini加入扩展
```
  extension=swoole.so
```
4. 在laravel项目目录放入laravel_swoole.php
```
  php laravel_swoole.php
```

#### 设置
由于swoole不能直接提供静态文件下载，也许是我目前没找到方法所以先用nginx来代理。
```
  upstream web_app {
    server 127.0.0.1:9501;
  }

  server {
      listen       80;
      server_name  localhost;
      root /home/www/web_app/public;

      #access_log  /var/log/nginx/log/host.access.log  main;

      location / {
          proxy_set_header Host $host;
          proxy_redirect off;
          proxy_set_header X-Real-IP $remote_addr;
          proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
          proxy_pass http://web_app;
      }

      location ~ ^/js/ {
          expires 1y;
          add_header Cache-Control public;

          add_header ETag "";
          break;
      }

      location ~ ^/css/ {
          expires 1y;
          add_header Cache-Control public;

          add_header ETag "";
          break;
      }

      location ~ /\.ht {
          deny  all;
      }
  }
```

#### 优缺点
1. 使用swoole代理laravel访问速度飙升.
2. 上传文件暂时不可用.
3. 数据库操作未长时间测试，一般的curd没发现什么问题.
